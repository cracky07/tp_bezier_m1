# How to install

Use your favorite IDE for python, i recommend Visual Studio Code 

Go to a repository and in your terminal type :
```
git clone https://gitlab.com/cracky07/tp_bezier_m1.git
```

Then go to project repository 

```
cd projet/path
```

Create python virtual environment and install project dependencies.


Go in virtual environment 

```
source venv/bin/activate
```
When you open the project always install requirements to have dependencies up to date

```
pip install -r requirements.txt
```

Now you can execute code

```
python bezier_curve.py
python bezier_surface.py
```

# Results example

Curve

![](images/curve.png)

Surface

![](images/surface.png)