import numpy as np
import matplotlib.pyplot as plt
from utils.utils import bernstein_polynomial, generate_random_control_points

def bezier_curve(control_points, num_points=50):
    """
    Compute a Bezier curve from a set of control points in 2D.
    
    Parameters:
    - control_points: An array of control points, each with x, y coordinates.
    - num_points: The number of points to compute along the curve.
    
    Returns:
    - A numpy array representing the points along the Bezier curve.
    """
    n = len(control_points) - 1
    t = np.linspace(0, 1, num_points)
    curve = np.zeros((num_points, 2))
    for i, point in enumerate(control_points):
        bernstein_poly = bernstein_polynomial(i, n, t)
        curve += np.outer(bernstein_poly, point)
    return curve

def plot_curve(curve):
    """
    Plot a Bezier curve in 2D.
    
    Parameters:
    - curve: A numpy array representing the points along the Bezier curve.
    """
    plt.plot(curve[:,0], curve[:,1], 'r')
    
def plot_control_points(control_points):
    """
    Plot the control points for the Bezier curve in 2D.
    
    Parameters:
    - control_points: An array of control points, each with x, y coordinates.
    """
    plt.scatter(control_points[:,0], control_points[:,1], color='blue')
    plt.plot(control_points[:,0], control_points[:,1], 'bo-')
    

def main():
    """
    Main function to demonstrate computing and plotting a Bezier curve in 2D.
    """
    # control_points = np.array([
    #     [0, 0],
    #     [1, 2],
    #     [4, 3],
    #     [3, 0],
    #     [5, 1]
    # ])
    
    control_points = generate_random_control_points(10, 2)
    
    curve = bezier_curve(control_points)
    plt.figure()
    plot_curve(curve)
    plot_control_points(control_points)
    plt.show()

if __name__ == "__main__":
    main()
