"""
CC-BY kevin.molina@univ-amu.fr - 18/02/2024
"""

import numpy as np
import matplotlib.pyplot as plt
from utils.utils import bernstein_polynomial

def bezier_surface(control_points, num_points_u=50, num_points_v=50):
    """
    Compute a Bezier surface given a grid of control points.

    Parameters:
    - control_points: A numpy array of shape (n+1, m+1, 3) representing the control points grid.
    - num_points_u: Number of points to compute along the u dimension.
    - num_points_v: Number of points to compute along the v dimension.

    Returns:
    - A numpy array representing the computed Bezier surface.
    """
    # Determine the degree of the surface in both dimensions
    n = control_points.shape[0] - 1
    m = control_points.shape[1] - 1
    # Generate linearly spaced points between 0 and 1 for both u and v parameters
    u = np.linspace(0, 1, num_points_u)
    v = np.linspace(0, 1, num_points_v)
    # Initialize an array to store the surface points
    surface = np.zeros((num_points_u, num_points_v, 3))
    
    # Iterate over the control points and compute the surface using Bernstein polynomials
    for i in range(n + 1):
        for j in range(m + 1):
            # Calculate Bernstein polynomials for current control point in u and v directions
            bernstein_u = bernstein_polynomial(i, n, u)
            bernstein_v = bernstein_polynomial(j, m, v)
            for k in range(3):  # Iterate over x, y, z coordinates
                # Update the surface points by adding the contribution of the current control point
                surface[:, :, k] += np.outer(bernstein_u, bernstein_v) * control_points[i, j, k]
    
    return surface

def extract_coordinates(surface):
    """
    Extract the x, y, and z coordinates from a Bezier surface.

    Parameters:
    - surface: A numpy array representing the computed Bezier surface.

    Returns:
    - Three numpy arrays representing the x, y, and z coordinates of the surface.
    """
    x = surface[:, :, 0]
    y = surface[:, :, 1]
    z = surface[:, :, 2]
    return x, y, z

def plot_surface(x, y, z, ax):
    """
    Plot a 3D surface using the given x, y, and z coordinates.

    Parameters:
    - x, y, z: Numpy arrays representing the x, y, and z coordinates of the surface.
    - ax: The matplotlib 3D axis on which to plot the surface.
    """
    # Plot the surface with specified appearance settings
    ax.plot_surface(x, y, z, rstride=1, cstride=1, color='r', edgecolor='none')

def plot_control_grid(control_grid, ax):
    """
    Optionally plot the control points grid for the Bezier surface.

    Parameters:
    - control_grid: A numpy array of shape (n+1, m+1, 3) representing the control points grid.
    - ax: The matplotlib 3D axis on which to plot the control points.
    """
    # Plot lines connecting control points in the grid
    for grid_line in control_grid:
        ax.plot(grid_line[:, 0], grid_line[:, 1], grid_line[:, 2], 'bo-')
    for grid_line in control_grid.transpose(1, 0, 2):
        ax.plot(grid_line[:, 0], grid_line[:, 1], grid_line[:, 2], 'bo-')

def main():
    """
    Main function to demonstrate the computation and plotting of a Bezier surface.
    """
    # Define a control points grid for the Bezier surface
    control_grid = np.array([
        [[0, 0, 0], [1, 2, 3], [4, 3, 2], [3, 0, 1]],
        [[1, 1, 1], [2, 3, 4], [5, 4, 3], [4, 1, 2]],
        [[2, 2, 2], [3, 4, 5], [6, 5, 4], [5, 2, 3]],
        [[3, 3, 3], [4, 5, 6], [7, 6, 5], [6, 3, 4]]
    ])
    
    # Compute the Bezier surface from the control grid
    surface = bezier_surface(control_grid)
    # Extract the x, y, z coordinates from the computed surface
    x, y, z = extract_coordinates(surface)
    
    # Set up the plot and axes for 3D visualization
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    # Plot the Bezier surface
    plot_surface(x, y, z, ax)
    # Optionally, plot the control points grid for reference
    plot_control_grid(control_grid, ax)
    # Display the plot
    plt.show()

if __name__ == "__main__":
    main()
