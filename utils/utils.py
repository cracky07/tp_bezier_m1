"""
CC-BY kevin.molina@univ-amu.fr - 18/02/2024
"""

from scipy.special import comb
import numpy as np

def bernstein_polynomial(i, n, t):
    """
    Compute the Bernstein polynomial of n, i as a function of t.
    
    Parameters:
    - i: The polynomial's index.
    - n: The degree of the polynomial.
    - t: The parameter value, can be a numpy array for multiple values.
    
    Returns:
    - The evaluated Bernstein polynomial.
    """
    return comb(n, i) * (t ** i) * ((1 - t) ** (n - i))

def generate_random_control_points(num_points, dimension):
    """
    Generate a random set of control points for the Bezier curve in 2D.
    
    Parameters:
    - num_points: The number of control points to generate.
    
    Returns:
    - A numpy array representing the generated control points.
    """
    return np.random.rand(num_points, dimension)