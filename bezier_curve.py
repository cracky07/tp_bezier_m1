"""
CC-BY kevin.molina@univ-amu.fr - 18/02/2024
"""

import numpy as np
import matplotlib.pyplot as plt
from utils.utils import bernstein_polynomial, generate_random_control_points

def bezier_curve(control_points, num_points=100):
    """
    Compute a Bezier curve from a set of control points.
    
    Parameters:
    - control_points: An array of control points, each with x, y, z coordinates.
    - num_points: The number of points to compute along the curve.
    
    Returns:
    - A numpy array representing the points along the Bezier curve.
    """
    # Determine the degree of the curve based on the number of control points
    n = len(control_points) - 1
    # Generate a linearly spaced array of parameter values between 0 and 1
    t = np.linspace(0, 1, num_points)
    # Initialize an array to store the computed points along the curve
    curve = np.zeros((num_points, 3))
    # Iterate over each control point and its index
    for i, point in enumerate(control_points):
        # Calculate the Bernstein polynomial for the current control point and parameter t
        bernstein_poly = bernstein_polynomial(i, n, t)
        # Add the contribution of the current control point to the curve
        curve += np.outer(bernstein_poly, point)
    return curve

def plot_curve(curve, ax):
    """
    Plot a Bezier curve on a given matplotlib 3D axis.
    
    Parameters:
    - curve: A numpy array representing the points along the Bezier curve.
    - ax: A matplotlib 3D axis object for plotting.
    """
    # Plot the curve as a red line
    ax.plot(curve[:,0], curve[:,1], curve[:,2], 'r')
    
def plot_control_points(control_points, ax):
    """
    Optionally plot the control points on a given matplotlib 3D axis.
    
    Parameters:
    - control_points: An array of control points, each with x, y, z coordinates.
    - ax: A matplotlib 3D axis object for plotting.
    """
    # Plot control points as blue dots
    ax.scatter(control_points[:,0], control_points[:,1], control_points[:,2], color='blue')
    # ax.scatter(control_points[0,0], control_points[0,1], control_points[0,2], color='blue')
    # ax.scatter(control_points[len(control_points) - 1,0], control_points[len(control_points) - 1,1], control_points[len(control_points) - 1,2], color='blue')
    # Plot lines connecting control points
    ax.plot(control_points[:,0], control_points[:,1], control_points[:,2], 'bo-')
    


def main():
    """
    Main function to demonstrate computing and plotting a Bezier curve.
    """
    # Define example control points for the Bezier curve
    # control_points = np.array([
    #     [0, 0, 0],
    #     [1, 2, 3],
    #     [4, 3, 2],
    #     [3, 0, 1]
    # ])
    
    control_points = generate_random_control_points(10, 3)
    
    # Compute the Bezier curve based on the control points
    curve = bezier_curve(control_points)
    # Initialize a matplotlib figure and 3D axis for plotting
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    # Plot the computed Bezier curve
    plot_curve(curve, ax)
    # Optionally, plot the control points to visualize the curve's construction
    plot_control_points(control_points, ax)
    # Display the plot
    plt.show()

# Check if this script is run as the main program and execute the main function
if __name__ == "__main__":
    main()
